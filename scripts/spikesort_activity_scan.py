"""
Template script to spike sort a list of Activity Scan recordings using SpikeInterface.

Author: Alessio Buccino
"""

import spikeextractors as se
import belextractors as be
import spiketoolkit as st
import spikesorters as ss
import spikewidgets as sw
import shutil
from pathlib import Path
import time
import numpy as np
from tqdm import tqdm
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.use("agg")

from spikesorting_utils import add_colorbar
import axon_velocity as av

############################################
################  INPUTS ###################
############################################


# list of data files you want to spike sort
activityscan_folders = ['/net/bs-filesvr02/export/group/hierlemann/recordings/Mea1k/vsivaraj/201013/2821/Activity_Scan']

# list of intermediate folder where tmp and output files are saved
intermediate_folders = ['/net/bs-filesvr02/export/group/hierlemann/intermediate_data/Mea1k/vsivaraj/201013/2821/Activity_Scan']


assert len(activityscan_folders) == len(intermediate_folders), "'activityscan_folders' and 'intermediate_folders' " \
                                                               "should have the same length"

# sorter name
sorter = 'kilosort2'
# sorter params
sorter_params = {'n_jobs_bin': 16, 'chunk_mb': 4000}

############################################
################  PARAMS ###################
############################################

# If True, and spike sorting output is present, it's deleted and resorted
recompute_sorting = False
recompute_curation = False

# If True, filtered data and sorted outputs are saved in a format that it's easy to retrieve (.pkl)
dump_recording = True
dump_sorting = True

# If True, exports to Phy
export_raw_to_phy = False
export_curated_to_phy = False

# If True, unit templates are plotted for all units
plot_unit_templates = False

### FILTER params ###
freq_min = 300
fre_max = 6000

### AUTOMATIC CURATION ###
# If True, output is automatically curated using quality metrics (QC)
auto_curate = True

# Thresholds for automatic curations (if any is None, that autocuration is skipped
# ISI-violation ratio (greater values are removed)
isi_viol_thresh = 0.5
# firing rate (smaller values are removed)
fr_thresh = 0.05
# signal-to-noise ratio (smaller values are removed)
snr_thresh = 5

# TODO: add more QCs

### OTHER PROCESSING PARAMS ###

# number of jobs to use
n_jobs = 16
# number of Mb to use for RAM (4000=4Gb)
chunk_mb = 4000
# Number of spikes per unit to compute templates (None-> all spikes are used)
max_spikes_per_unit = None
# Number of channels to compute center of mass
num_channels_for_com = 30

########################################################################################################################
# Let's do it!

# loop through folders
for (activity_scan, intermediate_folder) in zip(activityscan_folders, intermediate_folders):
    t_start_all = time.time()
    activity_scan = Path(activity_scan)
    intermediate_folder = Path(intermediate_folder)
    # set paths
    if not activity_scan.is_dir():
        print(f"{activity_scan} is not a folder! Skipping")
        continue
    else:
        print(f"Processing date {activity_scan}")

    output_base_folder = intermediate_folder / sorter
    cache_base_folder = intermediate_folder / 'cache'
    figures_base_folder = intermediate_folder / 'figures'
    cache_base_folder.mkdir(parents=True, exist_ok=True)

    # sort AS configs
    rec_paths = [p for p in activity_scan.iterdir() if p.suffix == '.h5']
    rec_ids = [int(rec.name.split('.')[0]) for rec in rec_paths]
    rec_paths = np.array(rec_paths)[np.argsort(rec_ids)]

    # process configs separately
    for curr, rec_path in enumerate(rec_paths):
        print(f"Config: {curr + 1}/{len(rec_paths)}")
        # Load recording
        try:
            print("Trying to load Mea1k recording")
            rec = be.Mea1kRecordingExtractor(rec_path, load_spikes=False)
        except:
            try:
                print("Trying to load MaxOne recording")
                rec = se.MaxOneRecordingExtractor(rec_path, load_spikes=False)
            except:
                try:
                    print("Trying to load MaxTwo format")
                    rec = se.MaxTwoRecordingExtractor(rec_path, load_spikes=False)
                except:
                    raise Exception(f"Could not open the provided file: {rec_path}")

        cache_folder = cache_base_folder / rec_path.name
        output_folder = output_base_folder / rec_path.name
        tmp_folder = cache_base_folder / 'tmp' / sorter
        tmp_folder.mkdir(parents=True, exist_ok=True)

        ### Filter and dumping
        if (cache_folder / 'recording.pkl').is_file() and (cache_folder / 'recording.dat').is_file():
            print("Loading cached recording")
            rec_cache = se.load_extractor_from_pickle(cache_folder / 'recording.pkl')
            fs = rec_cache.get_sampling_frequency()
        else:
            print('FILTERING\n')
            start = time.time()
            rec_f = st.preprocessing.bandpass_filter(rec, freq_min=freq_min, freq_max=fre_max, dtype='float32')
            rec_cache = se.CacheRecordingExtractor(rec_f, save_path=cache_folder / 'recording.dat',
                                                   chunk_mb=chunk_mb,
                                                   n_jobs=n_jobs)
            stop = time.time()
            print(f'Elapsed filter time {stop - start}\n')
            fs = rec_cache.get_sampling_frequency()

            if dump_recording:
                # Dump recording with activity
                rec_cache.dump_to_pickle(cache_folder / 'recording.pkl')
                print(f"Filtered recording dumped to {cache_folder / 'recording.pkl'}\n")

        ### Spike sorting
        if recompute_sorting and output_folder.is_dir():
            shutil.rmtree(output_folder)
        try:
            if not (cache_folder / 'sorting.npz').is_file():
                print(f'SORTING WITH {sorter}\n')
                t_start_sort = time.time()
                sorting = ss.run_sorter(sorter, rec_cache, output_folder=output_folder, verbose=True,
                                        **sorter_params)
                se.NpzSortingExtractor.write_sorting(sorting, save_path=str(cache_folder / 'sorting.npz'))
                print(f"\n\nSpike sorting elapsed time {time.time() - t_start_sort} s")

                if export_raw_to_phy:
                    st.postprocessing.export_to_phy(rec_cache, sorting, cache_folder / "phy_raw",
                                                    n_jobs=n_jobs, chunk_mb=chunk_mb)
            else:
                print('Skipping', rec_path, ' since already sorted')
                sorting = se.NpzSortingExtractor(cache_folder / 'sorting.npz')
        except Exception as e:
            print(f"{sorter} failed on recording {rec_path}\n\nError: {e}")
            continue

        ### Auto-curation ###
        if auto_curate:
            if (cache_folder / 'sorting_curated.pkl').is_file() and not recompute_curation:
                print("Loading cached sorting")
                sorting_curated = se.load_extractor_from_pickle(cache_folder / 'sorting_curated.pkl')
            else:
                print('CURATING\n')
                print(f'N units before curation: {len(sorting.get_unit_ids())}\n')
                if fr_thresh is not None:
                    print(f"Curation based on firing rate")
                    sorting_curated = st.curation.threshold_firing_rates(sorting, threshold=fr_thresh,
                                                                         threshold_sign='less',
                                                                         duration_in_frames=
                                                                         rec_cache.get_num_frames())
                    print(f'N units after num spikes curation: {len(sorting_curated.get_unit_ids())}\n')
                else:
                    sorting_curated = sorting

                if fr_thresh is not None:
                    if len(sorting_curated.get_unit_ids()) > 0:
                        print(f"Curation based on isi violation ratio")
                        sorting_curated = st.curation.threshold_isi_violations(sorting_curated,
                                                                               threshold=isi_viol_thresh,
                                                                               threshold_sign='greater',
                                                                               duration_in_frames=
                                                                               rec_cache.get_num_frames())
                        print(f'N units after ISI curation: {len(sorting_curated.get_unit_ids())}\n')
                    else:
                        print("No units remain after curation")

                if snr_thresh is not None:
                    if len(sorting_curated.get_unit_ids()) > 0:
                        print(f"Curation based on SNR (this might take some time...)")
                        # Compute waveforms and templates
                        sorting_curated.set_tmp_folder(tmp_folder)
                        wf = st.postprocessing.get_unit_waveforms(rec_cache, sorting_curated, chunk_mb=chunk_mb,
                                                                  max_spikes_per_unit=max_spikes_per_unit,
                                                                  n_jobs=n_jobs, verbose=True)

                        templates_curated = st.postprocessing.get_unit_templates(rec_cache, sorting_curated)

                        sorting_curated = st.curation.threshold_snrs(sorting_curated, rec_cache,
                                                                     threshold=snr_thresh,
                                                                     threshold_sign='less')
                        print(f'N units after SNR curation: {len(sorting_curated.get_unit_ids())}\n')
                    else:
                        print("No units remain after curation")

                # compute COM on templates
                coms = st.postprocessing.compute_unit_centers_of_mass(rec_cache, sorting_curated,
                                                                      num_channels=num_channels_for_com)

                feats = st.postprocessing.compute_unit_template_features(rec_cache, sorting_curated)

                if export_curated_to_phy:
                    st.postprocessing.export_to_phy(rec_cache, sorting_curated, cache_folder / "phy_curated",
                                                    n_jobs=n_jobs, chunk_mb=chunk_mb)


                if dump_sorting:
                    # Dump sorting with templates
                    sorting_curated.dump_to_pickle(cache_folder / 'sorting_curated.pkl', include_features=False)
                    print(f"Curated sorting dumped to {cache_folder / 'sorting_curated.pkl'}\n")
                    shutil.rmtree(tmp_folder, ignore_errors=True)
        else:
            sorting.set_tmp_folder(tmp_folder)
            # compute templates anyways
            wf = st.postprocessing.get_unit_waveforms(rec_cache, sorting, chunk_mb=chunk_mb,
                                                      max_spikes_per_unit=max_spikes_per_unit,
                                                      n_jobs=n_jobs, verbose=True)
            templates = st.postprocessing.get_unit_templates(rec_cache, sorting)

            # compute COM on templates
            coms = st.postprocessing.compute_unit_centers_of_mass(rec_cache, sorting,
                                                                  num_channels=num_channels_for_com)

            feats = st.postprocessing.compute_unit_template_features(rec_cache, sorting)

            if dump_sorting:
                # Dump sorting with templates
                sorting.dump_to_pickle(cache_folder / 'sorting.pkl', include_features=False)
                print(f"Curated sorting dumped to {cache_folder / 'sorting.pkl'}\n")
                shutil.rmtree(tmp_folder, ignore_errors=True)

    print(f"\n\nTotal filtering, sorting, curation elapsed time {time.time() - t_start_all} s")

    print(f"Plotting centers of mass figure")
    rec_folders = [p for p in cache_base_folder.iterdir() if p.is_dir() and 'raw' in p.name]
    # sort recordings
    rec_ids = [int(rec.name.split('.')[0]) for rec in rec_folders]
    rec_folders = np.array(rec_folders)[np.argsort(rec_ids)]

    recordings = []
    sortings = []
    for rec_folder in rec_folders:
        if (rec_folder / 'recording.pkl').is_file():
            recordings.append(se.load_extractor_from_pickle(rec_folder / 'recording.pkl'))

            if (rec_folder / 'sorting_curated.pkl').is_file():
                sortings.append(se.load_extractor_from_pickle(rec_folder / 'sorting_curated.pkl'))
            elif (rec_folder / 'sorting.pkl').is_file():
                sortings.append(se.load_extractor_from_pickle(rec_folder / 'sorting.pkl'))
            else:
                print(f"Skipping {rec_folder.name} because not complete")
        else:
            print(f"Skipping {rec_folder.name} because not complete")

    multirec = se.MultiRecordingChannelExtractor(recordings)
    multisort = se.MultiSortingExtractor(sortings)

    # figure with COM
    w_elec = sw.plot_electrode_geometry(multirec, color='y')
    cmap = 'coolwarm'
    cm = plt.get_cmap(cmap)
    n_units = len(multisort.get_unit_ids())

    # precompute template amplitudes
    template_amps = []
    for i_u, u in enumerate(multisort.get_unit_ids()):
        template = multisort.get_unit_property(u, 'template')
        amp = np.max(np.abs(template))
        template_amps.append(amp)

    norm_amps = np.array(template_amps) - np.min(template_amps)
    norm_amps /= np.ptp(template_amps)

    for i_u, u in enumerate(multisort.get_unit_ids()):
        com = multisort.get_unit_property(u, 'com')
        color = cm(norm_amps[i_u])
        w_elec.ax.plot(com[0], com[1], marker='o', color=color, markersize=5)

    colorbar = add_colorbar(multirec.get_channel_locations(), template_amps, w_elec, cmap)
    colorbar.set_label("Amplitude $\mu$V", rotation=90, labelpad=-20)

    x, y = multirec.get_channel_locations().T
    w_elec.ax.text(np.min(x) - 20, np.min(y) - 20, "0", fontsize=10)

    if not figures_base_folder.is_dir():
        figures_base_folder.mkdir(parents=True)
    w_elec.figure.savefig(figures_base_folder / 'com.png', dpi=600)

    if plot_unit_templates:
        figures_units = figures_base_folder / "units"
        figures_units.mkdir(parents=True, exist_ok=True)

        # TODO fix this
        for ir, (rec, sort) in enumerate(zip(recordings, sortings)):
            locations = rec.get_channel_locations()
            fs = rec.get_sampling_frequency()
            electrode_ids = np.array([rec.get_channel_property(ch, 'electrode') for ch in rec.get_channel_ids()])
            unit_ids = sort.get_unit_ids()
            for i_u in tqdm(np.arange(len(unit_ids)), ascii=True,
                            desc=f"Plotting unit templates config {rec_folders[ir].name}"):
                u = unit_ids[i_u]
                com = sort.get_unit_property(u, 'com')
                template = sort.get_unit_property(u, 'template')

                fig_name = f"rec{ir}_unit{u}.pdf"
                fig, axs = plt.subplots(ncols=2, figsize=(10, 6))
                max_chan = np.unravel_index(np.argmax(np.abs(template)), template.shape)[0]
                electrode_id = electrode_ids[max_chan]
                amp = np.round(np.max(np.abs(template)))

                av.plot_amplitude_map(template, locations, log=True, ax=axs[0])
                axs[0].set_title("Amplitude")
                av.plot_peak_latency_map(template, locations, ax=axs[1])
                axs[1].set_title("Latency")
                fig.suptitle(f"rec {ir} - unit {u} - elec {electrode_id} - amp {amp}$\mu$V\n"
                             f"com: {[np.round(com[0], 1), np.round(com[1], 1)]}")
                fig.savefig(str(figures_units / fig_name))
                plt.close(fig)
