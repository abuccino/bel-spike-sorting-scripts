"""
Template script to spike sort axon recordings using SpikeInterface.

Author: Alessio Buccino
"""

import belextractors as be
import spikeextractors as se
import spiketoolkit as st
import spikesorters as ss
import spikewidgets as sw
import shutil
from pathlib import Path
import time
import numpy as np
from tqdm import tqdm
import matplotlib as mpl
import matplotlib.pyplot as plt
from joblib import Parallel, delayed

mpl.use("agg")

from spikesorting_utils import add_colorbar, extract_one_full_template
import axon_velocity as av

############################################
################  INPUTS ###################
############################################

# list of axon scan folders you want to spike sort
axon_scan_folders = ['/net/bs-filesvr02/export/group/hierlemann/recordings/Mea1k/vsivaraj/201221/2927/Axon_Scan_Ctrl']

# list of intermediate folder where tmp and output files are saved
intermediate_folders = ['/net/bs-filesvr02/export/group/hierlemann/intermediate_data/Mea1k/vsivaraj/201221/2927/Axon_Scan_Ctrl']


assert len(axon_scan_folders) == len(intermediate_folders), "'axon_scan_folders' and 'intermediate_folders' " \
                                                            "should have the same length"

# sorter name
sorter = 'kilosort2'
# sorter params
sorter_params = {"n_jobs_bin": 16, "chunk_mb": 4000}

############################################
################  PARAMS ###################
############################################

# If True, and spike sorting output is present, it's deleted and resorted
recompute_sorting = False
recompute_curation = False
recompute_full_template = False

# If True, filtered data and sorted outputs are saved in a format that it's easy to retrieve (.pkl)
dump_recording = True
dump_sorting = True

# If True, exports to Phy
export_raw_to_phy = False
export_curated_to_phy = False

# If True, unit templates are plotted for all units
plot_unit_templates = True

### FILTER params ###
freq_min = 300
fre_max = 6000

### AUTOMATIC CURATION ###
# If True, output is automatically curated using quality metrics (QC)
auto_curate = True

# Thresholds for automatic curations (if any is None, that autocuration is skipped
# ISI-violation ratio (greater values are removed)
isi_viol_thresh = 0.5
# firing rate (smaller values are removed)
fr_thresh = 0.05
# signal-to-noise ratio (smaller values are removed)
snr_thresh = 5

# TODO: add more QCs

### OTHER PROCESSING PARAMS ###
# number of jobs to use
n_jobs = 16
# number of Mb to use for RAM (4000=4Gb)
chunk_mb = 4000
# Number of spikes per unit to compute templates (None-> all spikes are used)
max_spikes_per_unit = None
# Number of channels to compute center of mass
num_channels_for_com = 30


for (axon_scan_folder, intermediate_folder) in zip(axon_scan_folders, intermediate_folders):
    t_start_all = time.time()
    output_folder = intermediate_folder / 'sorted' / sorter
    cache_folder = intermediate_folder / 'cache'
    figures_folder = intermediate_folder / 'figures'
    figures_units = figures_folder / 'units'

    rec_paths = [p for p in axon_scan_folder.iterdir() if p.suffix == '.h5']
    # sort recordings
    rec_ids = [int(rec.name.split('.')[0]) for rec in rec_paths]
    rec_paths = np.array(rec_paths)[np.argsort(rec_ids)]

    ### Find common set of electrodes ###
    electrode_ids = []
    channel_ids = []
    recordings = []
    for i, rec_path in enumerate(rec_paths):
        # Load recording
        try:
            print("Trying to load Mea1k recording")
            recording = be.Mea1kRecordingExtractor(rec_path, load_spikes=False)
        except:
            try:
                print("Trying to load MaxOne recording")
                recording = se.MaxOneRecordingExtractor(rec_path, load_spikes=False)
            except:
                try:
                    print("Trying to load MaxTwo format")
                    recording = se.MaxTwoRecordingExtractor(rec_path, load_spikes=False)
                except:
                    raise Exception(f"Could not open the provided file: {rec_path}")

        electrode_ids.append(recording.get_electrode_ids())
        channel_ids.append(recording.get_channel_ids())
        recordings.append(recording)

    fs = recordings[0].get_sampling_frequency()
    # find common set as intersection
    common_electrodes = set(electrode_ids[0])
    for elec in electrode_ids:
        common_electrodes = common_electrodes.intersection(elec)
    common_electrodes = list(common_electrodes)

    print(f"Number of common electrodes: {len(common_electrodes)}")
    # find channels corresponding to common set
    common_channel_ids = []
    for (chans, elecs) in zip(channel_ids, electrode_ids):
        elec_idxs = [elecs.index(el) for el in common_electrodes]
        common_channel_ids.append(list(np.array(chans)[elec_idxs]))

    ### Aggregate recordings ###
    recordings_common = []
    common_channel_list = list(np.arange(len(common_electrodes)))

    for i, rec in enumerate(recordings):
        recording_comm = se.SubRecordingExtractor(rec, channel_ids=common_channel_ids[i],
                                                  renamed_channel_ids=common_channel_list)
        recordings_common.append(recording_comm)

    epoch_names = [str(i) for i in range(len(recordings))]
    multirec = se.MultiRecordingTimeExtractor(recordings_common, epoch_names=epoch_names)
    multirec_f = st.preprocessing.bandpass_filter(multirec)

    # remove 10ms at transitions to avoid sorting artifacts
    transitions = []
    for ep in multirec.get_epoch_names()[:-1]:
        epoch = multirec.get_epoch_info(ep)
        transitions.append(epoch['end_frame'])
    multirec_rm = st.preprocessing.remove_artifacts(multirec_f, triggers=transitions, ms_before=10, ms_after=10)

    # Spike sort multirec
    print(f"\n\nProcessing multirec of {len(multirec.recordings)} configurations")

    tmp_folder = cache_folder / 'tmp'
    cache_folder.mkdir(parents=True, exist_ok=True)
    tmp_folder.mkdir(parents=True, exist_ok=True)

    ### Spike sorting ###
    if recompute_sorting and output_folder.is_dir():
        shutil.rmtree(output_folder)
    try:
        if not (cache_folder / 'sorting.npz').is_file():
            print(f'SORTING WITH {sorter}\n')
            t_start = time.time()
            sorting = ss.run_sorter(sorter, multirec_rm, output_folder=output_folder, verbose=True, **sorter_params)
            se.NpzSortingExtractor.write_sorting(sorting, save_path=str(cache_folder / 'sorting.npz'))
            t_stop = time.time()
            print(f"Elapsed time spike sorting {t_stop - t_start}")
        else:
            print('Skipping  since already sorted')
            sorting = se.NpzSortingExtractor(cache_folder / 'sorting.npz')
    except:
        print(f"{sorter} failed on axon scan {axon_scan_folder}")
        continue

    if export_raw_to_phy and not (cache_folder / "phy_raw").is_folder():
        print("Exporting raw sorting output to Phy")
        st.postprocessing.export_to_phy(multirec_rm, sorting, cache_folder / "phy_raw",
                                        n_jobs=n_jobs, chunk_mb=chunk_mb)

    ### Curation ###
    if auto_curate:
        ### Filter
        if (cache_folder / 'sorting_curated.pkl').is_file() and not recompute_curation:
            print("Loading cached sorting")
            sort_curated = se.load_extractor_from_pickle(cache_folder / 'sorting_curated.pkl')
        else:
            print('CURATING\n')
            t_start = time.time()
            print(f'N units before curation: {len(sorting.get_unit_ids())}\n')

            # Automatic curation based on spike trains
            if fr_thresh is not None:
                print(f"Curation based on firing rate and isi_violations")
                sorting_curated = st.curation.threshold_firing_rates(sorting, threshold=fr_thresh,
                                                                     threshold_sign='less',
                                                                     duration_in_frames=
                                                                     multirec.get_num_frames())
                print(f'N units after num spikes curation: {len(sorting_curated.get_unit_ids())}\n')
            else:
                sort_curated = sorting_curated

            if isi_viol_thresh is not None:
                if len(sorting_curated.get_unit_ids()) > 0:
                    sorting_curated = st.curation.threshold_isi_violations(sorting_curated, threshold=isi_viol_thresh,
                                                                           threshold_sign='greater',
                                                                           duration_in_frames=
                                                                           multirec.get_num_frames())
                print(f'N units after ISI curation: {len(sorting_curated.get_unit_ids())}\n')

            if snr_thresh is not None:
                if len(sorting_curated.get_unit_ids()) > 0:
                    # Compute waveforms and templates
                    print(f"Computing waveforms")
                    start = time.time()
                    sorting_curated.set_tmp_folder(tmp_folder)
                    wf = st.postprocessing.get_unit_waveforms(multirec_rm, sorting_curated, max_spikes_per_unit=300,
                                                              n_jobs=8, memmap=True, verbose=True)
                    end = time.time()
                    print(f'Elapsed waveform extraction time: {end - start}\n')

                    print(f"Computing curated templates\n")
                    templates_curated = st.postprocessing.get_unit_templates(multirec_rm, sorting_curated)

                    # Automatic curation based on SNR
                    print(f"Curation based on SNR")
                    sorting_curated = st.curation.threshold_snrs(sorting_curated, multirec_rm, threshold=snr_thresh,
                                                              threshold_sign='less')

                    print(f'N units after SNR curation: {len(sorting_curated.get_unit_ids())}\n')
            else:
                print("No units remain after curation")

            # compute other template features
            # feats = st.postprocessing.compute_unit_template_features(multirec_rm, sort_curated, upsample=5)

            if export_curated_to_phy:
                st.postprocessing.export_to_phy(multirec_rm, sorting_curated, cache_folder / "phy_curated",
                                                n_jobs=n_jobs, chunk_mb=chunk_mb)

            if dump_sorting:
                # Dump sorting with templates
                sorting_curated.dump_to_pickle(cache_folder / 'sorting_curated.pkl', include_features=False)
                print(f"Curated sorting dumped to {cache_folder / 'sorting_curated.pkl'}\n")
                shutil.rmtree(tmp_folder, ignore_errors=True)

            t_stop = time.time()
            print(f"Elapsed time curation {t_stop - t_start}")
    else:
        sorting.set_tmp_folder(tmp_folder)
        # compute templates anyways
        wf = st.postprocessing.get_unit_waveforms(multirec_rm, sorting, chunk_mb=chunk_mb,
                                                  max_spikes_per_unit=max_spikes_per_unit,
                                                  n_jobs=n_jobs, verbose=True)
        templates = st.postprocessing.get_unit_templates(multirec_rm, sorting)

        # compute COM on templates
        coms = st.postprocessing.compute_unit_centers_of_mass(multirec_rm, sorting,
                                                              num_channels=num_channels_for_com)

        feats = st.postprocessing.compute_unit_template_features(multirec_rm, sorting)

        if dump_sorting:
            # Dump sorting with templates
            sorting.dump_to_pickle(cache_folder / 'sorting.pkl', include_features=False)
            print(f"Curated sorting dumped to {cache_folder / 'sorting.pkl'}\n")
            shutil.rmtree(tmp_folder, ignore_errors=True)


    ### Split sortings ###
    if not (cache_folder / 'sorting_0.npz').is_file():
        print("Splitting sortings")
        t_start = time.time()
        multisorting = se.NpzSortingExtractor((cache_folder / 'sorting.npz'))
        curated = False
        if (cache_folder / 'sorting_curated.pkl').is_file():
            multisorting_curated = se.load_extractor_from_pickle(cache_folder / 'sorting_curated.pkl')
            curated = True

        # get single sortings and remove empty clusters
        sortings = []
        sortings_curated = []
        epochs = multirec.get_epoch_names()
        for ep in epochs:
            info = multirec.get_epoch_info(ep)

            start_frame = info['start_frame']
            end_frame = info['end_frame']

            sorting = se.SubSortingExtractor(multisorting, start_frame=start_frame, end_frame=end_frame)
            sorting = st.curation.threshold_num_spikes(sorting, 1, 'less')  # remove units with no spikes in epoch
            sortings.append(sorting)

            if curated:
                sorting_curated = se.SubSortingExtractor(multisorting_curated, start_frame=start_frame,
                                                      end_frame=end_frame)
                sorting_curated = st.curation.threshold_num_spikes(sorting_curated, 1, 'less')  # remove units with no spikes in epoch
                sortings_curated.append(sorting_curated)

        # save output
        for i, (sorting, sorting_curated) in enumerate(zip(sortings, sortings_curated)):
            se.NpzSortingExtractor.write_sorting(sorting, cache_folder / f'sorting_{i}.npz')
            se.NpzSortingExtractor.write_sorting(sorting_curated, cache_folder / f'sorting_curated_{i}.npz')

        t_stop = time.time()
        print(f"Elapsed time splitting {t_stop - t_start}")

    ### Extract full templates ###

    # load templates and locations
    locations = []
    for i, rec in enumerate(recordings):
        locs = rec.get_channel_locations()
        locations.append(locs)

    # find common locations and merge templates
    locations_set = [set(list([tuple(l) for l in locs])) for locs in locations]

    for l, lset in enumerate(locations_set):
        if l == 0:
            loc_union = lset
            loc_intersect = lset
        else:
            loc_union = loc_union | lset
            loc_intersect = loc_intersect & lset

    locations_all = np.array(list(loc_union))
    print(f"All locations: {len(loc_union)}, Intersection among configurations: {len(loc_intersect)}")
    # create dummy recording to get all the locations
    rec_dummy = np.zeros((len(locations_all), 5))
    rec_all_dummy = se.NumpyRecordingExtractor(rec_dummy, sampling_frequency=fs)
    rec_all_dummy.set_channel_locations(locations_all)
    rec_all_dummy = se.CacheRecordingExtractor(rec_all_dummy, save_path=cache_folder / 'recording_dummy.dat')
    rec_all_dummy.dump_to_pickle(cache_folder / 'recording_dummy.pkl')
    x, y = rec_all_dummy.get_channel_locations().T

    if (cache_folder / 'sorting_full_templates.pkl').is_file() and not recompute_full_template:
        print("Loading cached sorting wiht full templates")
        sort_curated = se.load_extractor_from_pickle(cache_folder / 'sorting_full_templates.pkl')
    else:
        print("Extracting full templates")
        t_start = time.time()
        sorting_files = [p for p in cache_folder.iterdir() if 'sorting_curated' in p.name and p.suffix == '.npz']
        if len(sorting_files) == 0:
            # auto curate False
            sorting_files = [p for p in cache_folder.iterdir() if 'sorting' in p.name and p.suffix == '.npz']
        sort_ids = [int(sort.stem.split('_')[-1]) for sort in sorting_files]
        sorting_files = np.array(sorting_files)[np.argsort(sort_ids)]

        sortings = []
        units = []
        for i, sort_file in enumerate(sorting_files):
            sort = se.NpzSortingExtractor(sort_file)
            sortings.append(sort)

        if (cache_folder / 'sorting_curated.pkl').is_file():
            sorting = se.load_extractor_from_pickle(cache_folder / 'sorting_curated.pkl')
        else:
            sorting = se.load_extractor_from_pickle(cache_folder / 'sorting.pkl')

        # load templates and locations
        rec_filtered = []
        for i, rec in enumerate(recordings):
            rec_filt = st.preprocessing.bandpass_filter(rec)
            rec_filtered.append(rec_filt)

        # Build dictionary with templates and locations for each config
        template_dict = {}
        for u in sorting.get_unit_ids():
            template_dict[u] = {'templates': [],
                                'locations': []}

        for i in np.arange(len(rec_filtered)):
            sorting_tmp_file = cache_folder / f'sorting_templates_{i}.pkl'
            recf = rec_filtered[i]
            sort_file = sorting_files[i]

            if sorting_tmp_file.is_file():
                print("Loading cached sortings with templates")
                sort = se.load_extractor_from_pickle(sorting_tmp_file)
                temps = [sort.get_unit_property(u, 'template') for u in sort.get_unit_ids()]
            else:
                print(f"Extracting templates for recording {i + 1} / {len(rec_filtered)}")
                sort = se.NpzSortingExtractor(sort_file)
                temps = st.postprocessing.get_unit_templates(recf, sort, max_spikes_per_unit=max_spikes_per_unit,
                                                             chunk_mb=chunk_mb, n_jobs=n_jobs,
                                                             verbose=True)
                sort.dump_to_pickle(sorting_tmp_file, include_features=False)

            if i == 0:
                template_shape = temps[0].shape
            for (u, temp) in zip(sort.get_unit_ids(), temps):
                template_dict[u]['templates'].append(temp)
                template_dict[u]['locations'].append(recf.get_channel_locations())

        # allocate memmap objects for parallel processing
        full_templates = np.memmap(filename=cache_folder / 'full_templates.raw',
                                   mode='w+', dtype='float',
                                   shape=(len(sorting.get_unit_ids()), len(loc_union), template_shape[1]))
        shared_channels = np.memmap(filename=cache_folder / 'shared_channels.raw',
                                    mode='w+', dtype='int',
                                    shape=(len(sorting.get_unit_ids()), len(loc_union)))

        # extract templates in parallel
        Parallel(n_jobs=n_jobs)(
            delayed(extract_one_full_template)(i, template_dict[key], locations_all, full_templates,
                                               shared_channels, True)
            for i, key in enumerate(list(template_dict.keys())))

        # load template as property
        for (u, template) in zip(sorting.get_unit_ids(), full_templates):
            sorting.set_unit_property(u, 'template', template)

        # retrieve all COMs
        coms = st.postprocessing.compute_unit_centers_of_mass(rec_all_dummy, sorting,
                                                              num_channels=num_channels_for_com)

        sorting.dump_to_pickle(cache_folder / 'sorting_full_templates.pkl', include_features=False)
        t_stop = time.time()
        print(f"Elapsed time full template extraction: {t_stop - t_start}s")

    print(f"Total units {len(sorting.get_unit_ids())}")

    # figure with axon tracks
    print(f"Plotting centers of mass figure")
    t_start_plot = time.time()
    fig_units = []
    figures_folder.mkdir(parents=True, exist_ok=True)
    figures_units.mkdir(parents=True, exist_ok=True)

    # figure with COM
    w_elec = sw.plot_electrode_geometry(rec_all_dummy, color='y')
    cmap = 'coolwarm'
    cm = plt.get_cmap(cmap)
    n_units = len(sorting.get_unit_ids())

    # precompute template amplitudes
    template_amps = []
    for i_u, u in enumerate(sorting.get_unit_ids()):
        template = sorting.get_unit_property(u, 'template')
        amp = np.max(np.abs(template))
        template_amps.append(amp)
    norm_amps = np.array(template_amps) - np.min(template_amps)
    norm_amps /= np.ptp(template_amps)
    for i_u, u in enumerate(sorting.get_unit_ids()):
        com = sorting.get_unit_property(u, 'com')
        color = cm(norm_amps[i_u])
        w_elec.ax.plot(com[0], com[1], marker='o', color=color, markersize=5)

    colorbar = add_colorbar(rec_all_dummy.get_channel_locations(), template_amps, w_elec, cmap)
    colorbar.set_label("Amplitude $\mu$V", rotation=90, labelpad=-20)
    x, y = rec_all_dummy.get_channel_locations().T
    w_elec.ax.text(np.min(x) - 20, np.min(y) - 20, "0", fontsize=10)
    w_elec.figure.savefig(figures_folder / 'com.png', dpi=600)

    if plot_unit_templates:
        figures_units = figures_folder / "units"
        figures_units.mkdir(parents=True, exist_ok=True)

        locations = rec_all_dummy.get_channel_locations()
        fs = rec_all_dummy.get_sampling_frequency()
        unit_ids = sorting.get_unit_ids()
        for i_u in tqdm(np.arange(len(unit_ids)), ascii=True,
                        desc=f"Plotting unit templates"):
            u = unit_ids[i_u]
            com = np.round(np.array(sorting.get_unit_property(u, 'com')), 1)
            template = sorting.get_unit_property(u, 'template')

            fig_name = f"unit{u}.pdf"
            fig, axs = plt.subplots(ncols=2, figsize=(10, 6))
            max_chan = np.unravel_index(np.argmax(np.abs(template)), template.shape)[0]
            amp = np.round(np.max(np.abs(template)))

            av.plot_amplitude_map(template, locations, log=True, ax=axs[0])
            axs[0].set_title("Amplitude")
            av.plot_peak_latency_map(template, locations, ax=axs[1])
            axs[1].set_title("Latency")
            fig.suptitle(f"unit {u} - amp {amp}$\mu$V\n"
                         f"com: {com}")
            fig.savefig(str(figures_units / fig_name))
            plt.close(fig)

    t_stop_plot = time.time()
    print(f"\n\nTotal plotting time {np.round(t_stop_plot - t_start_plot, 2)} s")

    t_stop_all = time.time()
    print(f"\n\nTotal elapsed time {t_stop_all - t_start_all} s")
