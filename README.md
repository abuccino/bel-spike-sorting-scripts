# BEL-spike-sorting-scripts

Collection of template scripts using [SpikeInterface](https://spikeinterface.readthedocs.io/en/latest/) to perform 
spike sorting at BEL.

# Setting up

### Prepare your server environment

To get started with spike sorting, ssh to a hierlemann server and modify the `.bashrc`:
 
```bash
ssh abuccino@bs-hierlemann09.ethz.ch

# modify .bashrc
nano ~/.bashrc

```

Add the following lines:
```bash
alias spikesort="source /net/bs-filesvr01/export/group/hierlemann/spikesorting/.spikesortingrc"
alias spikesortgpu="source /net/bs-filesvr01/export/group/hierlemann/spikesorting/.spikesorting_gpurc"
```

Press `ctrl+x` and then `y` to save.

Then restart the terminal or run `source ~/.bashrc` to apply the changes. 


You can now activate a conda environment called `spikesorting` with all dependencies and spikesorters installed by running:
```bash
spikesort # for non-GPU machines

spikesortgpu # for GPU-machines - bs-gpu02/bs-gpu03
``` 

Make sure the right `python` environment is activated by running `which python`:

```bash
which python

>>> /net/bs-filesvr01/export/group/hierlemann/spikesorting/miniconda3/envs/spikesorting/bin/python
```


### Modify the example scripts

You can find the template scripts already on the server at: `/net/bs-filesvr01/export/group/hierlemann/spikesorting/bel-spike-sorting-scripts`

Copy them in your favourite location and modify the header to provide:

- paths to folder/files to sort
- paths to intermediate folders to save output
- parameters for spike sorting (`sorter`, `sorter_params`)
- flags to: recompute, export to phy, perform automatic curation
- parameters for pre-processing and automatic curation

You can find more information in the comments of the scripts.

### Run the scripts

Once you have copied and modified the script, e.g. in my case it's in `/net/bs-filesvr01/export/group/hierlemann/Temp/Alessio/scripts`,
you can run it with:

```bash
cd /net/bs-filesvr01/export/group/hierlemann/Temp/Alessio/scripts
python spikesort_single_recording.py
```

### Output files

The intermediate folder that you indicated in the script will be organized as follows:

- intermediate_folder 
    - cache (cached files for recording and sorting - `.pkl`)
    - sorted / `sorter_name` (spike sorting output files)
    - figures  
        - `com.png`  (figure with centers of mass and amplitude of units)
        -  units     (folder with template/footprint figures for each sorted unit)

### Available sorters

SpikeInterface is now setup on the servers and it is interfaced with the following spike sorting packages:

- Kilosort2 (recommended) -- requires GPU
- Kilosort2.5 -- requires GPU
- HDsort (recommended)
- Ironclust (recommended)
- Spyking-circus (recommended)
- Tridesclous (recommended)
- Herdingspikes (recommended)
- Mountainsot4
- Klusta
- Wave-clus

The recommended sorters are the ones that should handle recordings form the Mea1k and NeuroCMOS probes (large-scale).
For DualMode, you will need to select which electrodes to sort.

To retrieve the list of available sorter-specific params, launch `ipython`:

```bash
ipython
```

and run (e.g. for HDSort):

```python
import spikesorters as ss

print(ss.installed_sorters())
print(ss.get_params_description("hdsort"))
print(ss.get_default_params("hdsort"))
```


# Contact

For further information and tutorials, refer to the online documentation .
For issues/questions/comments, contact Alessio Buccino (alessio.buccino@bsse.ethz.ch).

Have fun spike sorting!